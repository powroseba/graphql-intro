package com.intro.graphql.dealers;

import com.intro.graphql.commons.AbstractEntity;
import lombok.Getter;

@Getter
public class DealerEntity extends AbstractEntity<Long> {

    String name;
    String nip;
    String address;
    Long money;

    public DealerEntity(String name, String nip, String address, Long money) {
        this.name = name;
        this.nip = nip;
        this.address = address;
        this.money = money;
    }
}
