package com.intro.graphql.products;

import com.intro.graphql.commons.LongIdInMemoryRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.stream.Collectors;

@Repository
public class ProductInMemoryRepository extends LongIdInMemoryRepository<ProductEntity> {
    public Collection<ProductEntity> findAllByDealer_Id(Long id) {
        return entityCollection.values().stream()
                .filter(product -> product.getDealer().getId().equals(id))
                .collect(Collectors.toSet());
    }
}
