package com.intro.graphql.commons;

public abstract class LongIdInMemoryRepository<E extends AbstractEntity<Long>> extends AbstractInMemoryRepository<E, Long> {

    @Override
    protected Long generateNewIdentifier() {
        return (long) entityCollection.size() + 1;
    }
}
