package com.intro.graphql.dealers;

import com.intro.graphql.commons.LongIdInMemoryRepository;
import com.intro.graphql.products.ProductEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
class DealerDaoAdapter implements DealerDaoPort {

    private final LongIdInMemoryRepository<DealerEntity> dealerRepository;
    private final LongIdInMemoryRepository<ProductEntity> productRepository;


    @Override
    public Collection<Dealer> fetchAll() {
        log.debug("Fetching all dealers");
        return dealerRepository.findAll().stream()
                .map(this::mapDealer)
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<Dealer> fetchOne(Long id) {
        log.debug("Fetching dealer with id {}", id);
        return dealerRepository.findOne(id)
                .map(this::mapDealer);
    }

    public Dealer fetchDealerForProductById(Long id) {
        log.debug("Fetching dealer for product by id : {}", id);
        return productRepository.findOne(id)
                .map(ProductEntity::getDealer)
                .map(this::mapDealer)
                .orElse(null);
    }

    @Override
    public Dealer save(Dealer domainModel) {
        log.debug("Saving new dealer");
        final var entity = new DealerEntity(domainModel.getName(), domainModel.getNip(), domainModel.getAddress(), domainModel.getMoney());
        final var savedDealer =  dealerRepository.save(entity);
        return mapDealer(savedDealer);
    }

    private Dealer mapDealer(DealerEntity dealer) {
        return new Dealer(
                Optional.ofNullable(dealer).map(DealerEntity::getId).orElse(null),
                Optional.ofNullable(dealer).map(DealerEntity::getName).orElse(null),
                Optional.ofNullable(dealer).map(DealerEntity::getNip).orElse(null),
                Optional.ofNullable(dealer).map(DealerEntity::getAddress).orElse(null),
                Optional.ofNullable(dealer).map(DealerEntity::getMoney).orElse(null)
        );
    }
}
