package com.intro.graphql.clients;

import com.intro.graphql.commons.AbstractEntity;
import com.intro.graphql.commons.LongIdInMemoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
class ClientDaoAdapter implements ClientDaoPort {

    private final LongIdInMemoryRepository<ClientEntity> clientRepository;

    @Override
    public Collection<Client> fetchAll() {
        log.debug("Fetching all clients");
        return clientRepository.findAll().stream()
                .map(this::mapClient)
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<Client> fetchOne(Long id) {
        log.debug("Fetching client with id {}", id);
        return clientRepository.findOne(id).map(this::mapClient);
    }

    @Override
    public Client save(Client domainModel) {
        log.debug("Saving new client");
        final var entity = new ClientEntity(domainModel.getName(), domainModel.getMoney());
        final var savedClient = clientRepository.save(entity);
        return mapClient(savedClient);
    }

    private Client mapClient(ClientEntity client) {
        return new Client(
                Optional.ofNullable(client).map(AbstractEntity::getId).orElse(null),
                Optional.ofNullable(client).map(ClientEntity::getName).orElse(null),
                Optional.ofNullable(client).map(ClientEntity::getMoney).orElse(null)
        );
    }
}
