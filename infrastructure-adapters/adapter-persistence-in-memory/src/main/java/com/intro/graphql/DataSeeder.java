package com.intro.graphql;

import com.intro.graphql.clients.ClientEntity;
import com.intro.graphql.commons.LongIdInMemoryRepository;
import com.intro.graphql.dealers.DealerEntity;
import com.intro.graphql.products.ProductEntity;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
class DataSeeder {

    private final LongIdInMemoryRepository<ClientEntity> clientRepository;
    private final LongIdInMemoryRepository<ProductEntity> productRepository;
    private final LongIdInMemoryRepository<DealerEntity> dealerRepository;

    public DataSeeder(LongIdInMemoryRepository<ClientEntity> clientRepository,
                      LongIdInMemoryRepository<ProductEntity> productRepository,
                      LongIdInMemoryRepository<DealerEntity> dealerRepository) {
        this.clientRepository = clientRepository;
        this.productRepository = productRepository;
        this.dealerRepository = dealerRepository;
        injectEntities();
    }

    public void injectEntities() {
        Arrays.asList(
            new ClientEntity("Frank", 2000L),
            new ClientEntity("Tommy", 5000L),
            new ClientEntity("Anna", 4500L),
            new ClientEntity("Claire", 6000L),
            new ClientEntity("Carl", 10000L)
        ).forEach(clientRepository::save);


        var dealer = dealerRepository.save(new DealerEntity("Media Markt", "1132470708", "aleja Tadeusza Rejtana 36, 35-310 Rzeszów", 1500000L));
        var dealer1 = dealerRepository.save(new DealerEntity("RTV EURO AGD", "5270005984", "aleja Tadeusza Rejtana 65, 35-959 Rzeszów", 200000L));
        var dealer2 = dealerRepository.save(new DealerEntity("Media Expert", "7671004218", "aleja Majora Wacława Kopisto 1, 35-315 Rzeszów", 1000000L));

        Arrays.asList(
            new ProductEntity("Sennheiser HD 4.40 BT", 299L,  dealer1, null),
            new ProductEntity("Xiaomi Mi Box S", 299L, dealer1, null),
            new ProductEntity("Samsung UE55RU7472U", 2199L, dealer1, null),
            new ProductEntity("PANASONIC TX-58GX830E", 3099L, dealer, null),
            new ProductEntity("HUAWEI P30 Lite", 1299L, dealer, null),
            new ProductEntity("MSI Optix MAG241C", 1099L, dealer, null),
            new ProductEntity("XIAOMI Redmi Note 8T", 799L, dealer2, null),
            new ProductEntity("KENWOOD HMP34.A0WH", 219L, dealer2, null),
            new ProductEntity("PHILIPS LatteGo EP2231/40", 350L, dealer2, null)
        ).forEach(productRepository::save);
    }
}
