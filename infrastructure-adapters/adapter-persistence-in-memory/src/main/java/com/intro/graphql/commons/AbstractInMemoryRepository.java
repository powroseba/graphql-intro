package com.intro.graphql.commons;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public abstract class AbstractInMemoryRepository<E extends AbstractEntity<ID>, ID extends Serializable> {

    protected final ConcurrentHashMap<ID, E> entityCollection = new ConcurrentHashMap<>();

    public Collection<E> findAll() {
        return entityCollection.values();
    }

    public Optional<E> findOne(ID id) {
        return Optional.ofNullable(entityCollection.get(id));
    }

    public E save(E entity) {
        entity.setId(generateNewIdentifier());
        entityCollection.put(entity.getId(), entity);
        return entity;
    }

    protected abstract ID generateNewIdentifier();
}
