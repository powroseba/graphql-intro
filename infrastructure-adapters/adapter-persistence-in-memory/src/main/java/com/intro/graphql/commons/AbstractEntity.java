package com.intro.graphql.commons;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
public class AbstractEntity<T extends Serializable> {

    @Setter(AccessLevel.PACKAGE)
    private T id;
}
