package com.intro.graphql.clients;

import com.intro.graphql.commons.AbstractEntity;
import lombok.Getter;

@Getter
public class ClientEntity extends AbstractEntity<Long> {

    String name;
    Long money;

    public ClientEntity(String name, Long money) {
        this.name = name;
        this.money = money;
    }
}
