package com.intro.graphql.dealers;

import com.intro.graphql.commons.LongIdInMemoryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class DealerInMemoryRepository extends LongIdInMemoryRepository<DealerEntity> {

}
