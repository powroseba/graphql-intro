package com.intro.graphql.products;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
class ProductDaoAdapter implements ProductDaoPort {

    private final ProductInMemoryRepository productRepository;

    @Override
    public Collection<Product> fetchAll() {
        log.debug("Fetching products");
        return productRepository.findAll().stream()
                .map(this::mapProduct)
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Optional<Product> fetchOne(Long id) {
        log.debug("Fetching product with id : {}", id);
        return productRepository.findOne(id)
                .map(this::mapProduct);
    }

    @Override
    public Product save(Product domainModel) {
        log.debug("Saving new product");
        final var entity = new ProductEntity(domainModel.getName(), domainModel.getPrice(), null, null);
        final var savedProduct = productRepository.save(entity);
        return mapProduct(savedProduct);
    }

    public Collection<Product> fetchAllForDealerById(Long id) {
        return productRepository.findAllByDealer_Id(id).stream()
                .map(this::mapProduct)
                .collect(Collectors.toUnmodifiableSet());
    }

    private Product mapProduct(ProductEntity product) {
        return new Product(product.getId(), product.getName(), product.getPrice());
    }
}
