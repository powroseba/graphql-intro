package com.intro.graphql.clients;

import com.intro.graphql.commons.LongIdInMemoryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ClientInMemoryRepository extends LongIdInMemoryRepository<ClientEntity> {

}
