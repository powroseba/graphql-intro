package com.intro.graphql.products;

import com.intro.graphql.clients.ClientEntity;
import com.intro.graphql.commons.AbstractEntity;
import com.intro.graphql.dealers.DealerEntity;
import lombok.Getter;

@Getter
public class ProductEntity extends AbstractEntity<Long> {

    String name;
    Long price;
    DealerEntity dealer;
    ClientEntity client;

    public ProductEntity(String name, Long price, DealerEntity dealer, ClientEntity client) {
        this.name = name;
        this.price = price;
        this.dealer = dealer;
        this.client = client;
    }
}
