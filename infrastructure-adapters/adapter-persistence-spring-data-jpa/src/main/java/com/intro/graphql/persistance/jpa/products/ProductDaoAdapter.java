package com.intro.graphql.persistance.jpa.products;

import com.intro.graphql.products.ProductDaoPort;
import com.intro.graphql.products.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
class ProductDaoAdapter implements ProductDaoPort {

    private final ProductRepository productRepository;

    @Override
    public Collection<Product> fetchAll() {
        log.debug("Fetching products");
        return productRepository.findAll().stream()
                .map(product -> new Product(product.getId(), product.getName(), product.getPrice()))
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Optional<Product> fetchOne(Long id) {
        log.debug("Fetching product with id : {}", id);
        return productRepository.findById(id)
                .map(product -> new Product(product.getId(), product.getName(), product.getPrice()));
    }

    @Override
    public Product save(Product domainModel) {
        log.debug("Saving new product");
        var entity = new ProductEntity();
        entity.setName(domainModel.getName());
        entity.setPrice(domainModel.getPrice());
        entity = productRepository.save(entity);
        domainModel.setId(entity.getId());
        return domainModel;
    }
}
