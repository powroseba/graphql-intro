package com.intro.graphql.persistance.jpa.dealers;

import com.intro.graphql.persistance.jpa.commons.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "dealer")
@Data
@EqualsAndHashCode(callSuper = true)
public class DealerEntity extends AbstractEntity<Long> {

    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "nip", nullable = false)
    private String nip;
    @Column(name = "address", nullable = false)
    private String address;
    @Column(name = "money", nullable = false)
    private Long money;

}
