package com.intro.graphql.persistance.jpa.clients;

import com.intro.graphql.persistance.jpa.commons.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "client")
@Data
@EqualsAndHashCode(callSuper = true)
public class ClientEntity extends AbstractEntity<Long> {

    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "money", nullable = false)
    private Long money;
}
