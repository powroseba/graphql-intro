package com.intro.graphql.persistance.jpa.dealers;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DealerRepository extends JpaRepository<DealerEntity, Long> {
}
