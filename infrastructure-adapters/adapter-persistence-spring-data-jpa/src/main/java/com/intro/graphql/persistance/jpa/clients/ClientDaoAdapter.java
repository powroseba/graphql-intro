package com.intro.graphql.persistance.jpa.clients;

import com.intro.graphql.clients.ClientDaoPort;
import com.intro.graphql.clients.Client;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
class ClientDaoAdapter implements ClientDaoPort {

    private final ClientRepository clientRepository;

    @Override
    public Collection<Client> fetchAll() {
        log.debug("Fetching all clients");
        return clientRepository.findAll().stream()
                .map(client -> new Client(client.getId(), client.getName(), client.getMoney()))
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Optional<Client> fetchOne(Long id) {
        log.debug("Fetching client with id {}", id);
        return clientRepository.findById(id).map(client -> new Client(client.getId(), client.getName(), client.getMoney()));
    }

    @Override
    public Client save(Client domainModel) {
        log.debug("Saving new client");
        var entity = new ClientEntity();
        entity.setMoney(domainModel.getMoney());
        entity.setName(domainModel.getName());
        entity = clientRepository.save(entity);
        domainModel.setId(entity.getId());
        return domainModel;
    }
}
