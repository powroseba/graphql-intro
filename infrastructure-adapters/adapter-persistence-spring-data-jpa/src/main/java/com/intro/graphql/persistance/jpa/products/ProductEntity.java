package com.intro.graphql.persistance.jpa.products;

import com.intro.graphql.persistance.jpa.clients.ClientEntity;
import com.intro.graphql.persistance.jpa.commons.AbstractEntity;
import com.intro.graphql.persistance.jpa.dealers.DealerEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "product")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductEntity extends AbstractEntity<Long> {

    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "price", nullable = false)
    private Long price;
    @ManyToOne
    @JoinColumn(name = "dealer_id")
    private DealerEntity dealer;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private ClientEntity client;
}
