package com.intro.graphql.persistance.jpa.dealers;

import com.intro.graphql.dealers.DealerDaoPort;
import com.intro.graphql.dealers.Dealer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
class DealerDaoAdapter implements DealerDaoPort {

    private final DealerRepository dealerRepository;

    @Override
    public Collection<Dealer> fetchAll() {
        log.debug("Fetching all dealers");
        return dealerRepository.findAll().stream()
                .map(dealer -> new Dealer(dealer.getId(), dealer.getName(), dealer.getNip(), dealer.getAddress(), dealer.getMoney()))
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Optional<Dealer> fetchOne(Long id) {
        log.debug("Fetching dealer with id {}", id);
        return dealerRepository.findById(id)
                .map(dealer -> new Dealer(dealer.getId(), dealer.getName(), dealer.getNip(), dealer.getAddress(), dealer.getMoney()));
    }

    @Override
    public Dealer save(Dealer domainModel) {
        log.debug("Saving new dealer");
        var entity = new DealerEntity();
        entity.setName(domainModel.getName());
        entity.setNip(domainModel.getNip());
        entity.setAddress(domainModel.getAddress());
        entity.setMoney(domainModel.getMoney());
        entity = dealerRepository.save(entity);
        domainModel.setId(entity.getId());
        return domainModel;
    }
}
