package com.intro.graphql.products;

import com.intro.graphql.commons.AbstractDomainModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Product extends AbstractDomainModel<Long> {

    private String name;
    private Long price;

    public Product(Long id, String name, Long price) {
        super(id);
        this.name = name;
        this.price = price;
    }
}
