package com.intro.graphql.products;

import com.intro.graphql.commons.DomainModelDao;

public interface ProductDaoPort extends DomainModelDao<Product, Long> {
}
