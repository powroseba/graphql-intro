package com.intro.graphql.products;

import com.intro.graphql.commons.DomainModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService implements DomainModelService<Product, Long> {

    private final ProductDaoPort productDao;

    @Override
    @Transactional(readOnly = true)
    public Collection<Product> fetchAll() {
        log.debug("Fetching products ...");
        return productDao.fetchAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Product> fetchOne(Long id) {
        log.debug("Fetching product with id {} ...", id);
        return productDao.fetchOne(id);
    }

    @Override
    @Transactional
    public Product save(Product domainModel) {
        log.debug("Saving new product ...");
        return productDao.save(domainModel);
    }
}
