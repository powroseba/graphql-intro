package com.intro.graphql.commons;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public abstract class AbstractDomainModel<T extends Serializable> {

    private T id;

    public AbstractDomainModel(T id) {
        this.id = id;
    }
}
