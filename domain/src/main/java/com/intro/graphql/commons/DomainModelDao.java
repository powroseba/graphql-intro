package com.intro.graphql.commons;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;

public interface DomainModelDao<D extends AbstractDomainModel<T>, T extends Serializable> {

    Collection<D> fetchAll();

    Optional<D> fetchOne(T id);

    D save(D domainModel);
}
