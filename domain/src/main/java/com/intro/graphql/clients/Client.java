package com.intro.graphql.clients;

import com.intro.graphql.commons.AbstractDomainModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Client extends AbstractDomainModel<Long> {

    private String name;
    private Long money;


    public Client(Long id, String name, Long money) {
        super(id);
        this.name = name;
        this.money = money;
    }
}
