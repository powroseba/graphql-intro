package com.intro.graphql.clients;

import com.intro.graphql.commons.DomainModelDao;

public interface ClientDaoPort extends DomainModelDao<Client, Long> {
}
