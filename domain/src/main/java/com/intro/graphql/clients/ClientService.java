package com.intro.graphql.clients;

import com.intro.graphql.commons.DomainModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClientService implements DomainModelService<Client, Long> {

    private final ClientDaoPort clientDao;

    @Override
    @Transactional(readOnly = true)
    public Collection<Client> fetchAll() {
        log.debug("Fetching clients...");
        return clientDao.fetchAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Client> fetchOne(Long id) {
        log.debug("Fetching client with id {} ...", id);
        return clientDao.fetchOne(id);
    }

    @Override
    @Transactional
    public Client save(Client domainModel) {
        log.debug("Saving new client ...");
        return clientDao.save(domainModel);
    }
}
