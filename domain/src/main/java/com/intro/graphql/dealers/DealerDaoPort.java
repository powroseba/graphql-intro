package com.intro.graphql.dealers;

import com.intro.graphql.commons.DomainModelDao;

public interface DealerDaoPort extends DomainModelDao<Dealer, Long> {
}
