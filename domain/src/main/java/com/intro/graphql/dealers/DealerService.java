package com.intro.graphql.dealers;

import com.intro.graphql.commons.DomainModelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class DealerService implements DomainModelService<Dealer, Long> {

    private final DealerDaoPort dealerDao;

    @Override
    @Transactional(readOnly = true)
    public Collection<Dealer> fetchAll() {
        log.debug("Fetching dealers ...");
        return dealerDao.fetchAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Dealer> fetchOne(Long id) {
        log.debug("Fetching dealer with id {} ...", id);
        return dealerDao.fetchOne(id);
    }

    @Override
    @Transactional
    public Dealer save(Dealer domainModel) {
        log.debug("Saving new dealer ...");
        return dealerDao.save(domainModel);
    }
}
