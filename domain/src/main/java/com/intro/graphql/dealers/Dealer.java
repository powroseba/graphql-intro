package com.intro.graphql.dealers;

import com.intro.graphql.commons.AbstractDomainModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Dealer extends AbstractDomainModel<Long> {

    private String name;
    private String nip;
    private String address;
    private Long money;

    public Dealer(Long id, String name, String nip, String address, Long money) {
        super(id);
        this.name = name;
        this.nip = nip;
        this.address = address;
        this.money = money;
    }
}
